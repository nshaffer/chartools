program test_chartools
  use chartools
  implicit none
  
  call test__is_empty
  call test__is_blank
  call test__as_chars
  call test__as_string
  call test__starts_with
  call test__ends_with
  call test__contains_any
  call test__contains_all
  call test__upcase
  call test__downcase
  call test__strip_left
  call test__strip_right
  !call test__strip
  call test__squeeze
  call test__split
  call test__replace
  call test__capture
  call test__quote

contains

  subroutine test__is_empty
    character(len=*), parameter :: empty_string = ''
    character(len=*), parameter :: blank_string = '      '
    character(len=*), parameter :: mixed_string = '  foo bar '

    call banner("is_empty")

    write (*, '(A,T60,L8)') "Empty string '" // empty_string // "' is_empty? ", is_empty(empty_string)
    write (*, '(A,T60,L8)') "Blank string '" // blank_string // "' is_empty? ", is_empty(blank_string)
    write (*, '(A,T60,L8)') "Mixed string '" // mixed_string // "' is_empty? ", is_empty(mixed_string)
  end subroutine test__is_empty


  subroutine test__is_blank
    character(len=*), parameter :: empty_string = ''
    character(len=*), parameter :: blank_string = '      '
    character(len=*), parameter :: mixed_string = '  foo bar '

    call banner("is_blank")

    write (*, '(A,T60,L8)') "Empty string '" // empty_string // "' is_blank? ", is_blank(empty_string)
    write (*, '(A,T60,L8)') "Blank string '" // blank_string // "' is_blank? ", is_blank(blank_string)
    write (*, '(A,T60,L8)') "Mixed string '" // mixed_string // "' is_blank? ", is_blank(mixed_string)
  end subroutine test__is_blank


  subroutine test__as_chars
    character(len=*), parameter :: s = 'Fortran 90 > C++?   '
    character(len=1), dimension(:), allocatable :: c
    
    integer :: i = -1
    
    call banner("as_chars")

    c = as_chars(s)
    write (*, '(A)') "Original string: '" // s // "'"
    write (*, '(A)', advance='NO') "As characters: " 
    do i = 1, size(c)
       if (i > 1) write (*, '(A)', advance='NO') ","
       write (*, '(A)', advance='NO') "'" // c(i) // "'"
    end do
    write (*, *)
  end subroutine test__as_chars 


  subroutine test__as_string
    character(len=1), dimension(:), allocatable :: c
    character(len=:), allocatable :: s
    
    integer :: i = -1
    
    call banner("as_string")

    c = ['C','+','+',' ','<',' ','F','o','r','t','r','a','n',' ','9','0','?',' ',' ']
    s = as_string(c)
    write (*, '(A)', advance='NO') "Original characters: " 
    do i = 1, size(c)
       if (i > 1) write (*, '(A)', advance='NO') ","
       write (*, '(A)', advance='NO') "'" // c(i) // "'"
    end do
    write (*, *)
    write (*, '(A)') "As string: '" // s // "'"
  end subroutine test__as_string


  subroutine test__starts_with
    character(len=*), parameter :: s = 'Fortran 90'
    
    call banner("starts_with")
    
    write (*, '(A,T60,L8)') "The string '" // s // "' starts_with " // "'F'? ", starts_with(s, 'F')
    write (*, '(A,T60,L8)') "The string '" // s // "' starts_with " // "'For'? ", starts_with(s, 'Fortran')
    write (*, '(A,T60,L8)') "The string '" // s // "' starts_with " // "'Fortran '? ", starts_with(s, 'Fortran 90')
    write (*, '(A,T60,L8)') "The string '" // s // "' starts_with " // "''? ", starts_with(s, '')
    write (*, '(A,T60,L8)') "The string '" // s // "' starts_with " // "'90'? ", starts_with(s, '90')
  end subroutine test__starts_with


  subroutine test__ends_with
    character(len=*), parameter :: s = 'Fortran 90'
    
    call banner("ends_with")

    write (*, '(A,T60,L8)') "The string '" // s // "' ends_with '0'? ", ends_with(s, '0')
    write (*, '(A,T60,L8)') "The string '" // s // "' ends_with '90'? ", ends_with(s, '90')
    write (*, '(A,T60,L8)') "The string '" // s // "' ends_with 'Fortran 90'? ", ends_with(s, 'Fortran 90')
    write (*, '(A,T60,L8)') "The string '" // s // "' ends_with ''? ", ends_with(s, '')
    write (*, '(A,T60,L8)') "The string '" // s // "' ends_with 'For'? ", ends_with(s, 'For')
  end subroutine test__ends_with


  subroutine test__contains_any
    character(len=*), parameter :: s = 'Fortran 90'
    
    call banner("contains_any")
    
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_any of '09aF'?", contains_any(s, '09aF')
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_any of '0123456789'?", contains_any(s, '0123456789')
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_any of 'C+#'?", contains_any(s, 'C+#')
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_any of ''?", contains_any(s, '')

  end subroutine test__contains_any


  subroutine test__contains_all
    character(len=*), parameter :: s = 'Fortran 90'
    
    call banner("contains_all")
    
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_all of '09aF'?", contains_all(s, '09aF')
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_all of '0123456789'?", contains_all(s, '0123456789')
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_all of 'C+#'?", contains_all(s, 'C+#')
    write (*, '(A,T60,L8)') "The string '" // s // "' contains_all of ''?", contains_all(s, '')
  end subroutine test__contains_all


  subroutine test__upcase
    character(len=*), parameter :: s = 'Real programmers use Fortran!'

    call banner("upcase")

    write (*, '(A)') "Original: " // s
    write (*, '(A)') "After upcase: " // upcase(s)
  end subroutine test__upcase


  subroutine test__downcase
    character(len=*), parameter :: s = 'Complex programmers USE FORTRAN!'

    call banner("downcase")

    write (*, '(A)') "Original: " // s
    write (*, '(A)') "After downcase: " // downcase(s)
  end subroutine test__downcase


  subroutine test__strip_left
    character(len=*), parameter :: s = "    The quick, brown fox..."
    
    call banner("strip_left")

    write (*, '(A)') "Original string: '" // s // "'"
    write (*, '(A)') "strip_left ' ': '" // strip_left(s, ' ') // "'"
    write (*, '(A)') "strip_left 'T': '" // strip_left(s, 'T') // "'"
    write (*, '(A)') "strip_left ' ' then 'T': '" // strip_left(strip_left(s, ' '), 'T') // "'"
  end subroutine test__strip_left


  subroutine test__strip_right
    character(len=*), parameter :: s = "...jumped over the lazy dog.  "
    
    call banner("strip_right")

    write (*, '(A)') "Original string: '" // s // "'"
    write (*, '(A)') "strip_right ' ': '" // strip_right(s, ' ') // "'"
    write (*, '(A)') "strip_right 'og.': '" // strip_right(s, '.') // "'"
    write (*, '(A)') "strip_right ' ' then 'og.': '" // strip_right(strip_right(s, ' '), 'og.') // "'"
  end subroutine test__strip_right


  ! subroutine test__strip
  !   character(len=*), parameter :: s = "  The quick, brown fox...      "
  !  
  !   call banner("strip")
  !  
  !   write (*, '(A)') "Original string: '" // s // "'"
  !   write (*, '(A)') "strip ' ': '" // strip(s, ' ') // "'"
  !   write (*, '(A)') "strip '.': '" // strip(s, '.') // "'"
  !   write (*, '(A)') "strip ' ' then '.': '"  // strip(strip(s, ' '), '.') // "'"
  ! end subroutine test__strip


  subroutine test__squeeze
    character(len=*), parameter :: s = "  The     quick, brown  fox...      "
    
    call banner("squeeze")
    
    write (*, '(A)') "Original string: '" // s // "'"
    write (*, '(A)') "squeeze'd string: '" // squeeze(s) // "'"
    write (*, '(A)') "squeeze'd periods: '" // squeeze(s, '.') // "'"
  end subroutine test__squeeze


  subroutine test__split
    character(len=*), parameter :: s1 = "foo=3"
    character(len=*), parameter :: s2 = "The quick, brown fox"
    character(len=:), allocatable :: left, right
    
    call banner("split")
    
    call split(s1, "=", left, right)
    write (*, '(A)') "split '" // s1 // "' around '=' --> '" // left // "', '" // right // "'"
    
    if (allocated(left)) deallocate(left)
    if (allocated(right)) deallocate(right)
    call split(s1, "===", left, right)
    write (*, '(A)') "split '" // s1 // "' around '===' --> '" // left // "', '" // right // "'"

    if (allocated(left)) deallocate(left)
    if (allocated(right)) deallocate(right)
    call split(s2, "quick, ", left, right)
    write (*, '(A)') "split '" // s2 // "' around 'quick, ' --> '" // left // "', '" // right // "'"

    if (allocated(left)) deallocate(left)
    if (allocated(right)) deallocate(right)
    call split(s2, " ", left, right, back=.true.)
    write (*, '(A)') "split '" // s2 // "' around ' ' from back -- '" // left // "', '" // right // "'"
  end subroutine test__split


  subroutine test__replace
    character(len=*), parameter :: s = "The quick, brown fox"    
    
    call banner("replace")

    write (*, '(A)') "Original string: '" // s // "'"
    write (*, '(A)') "replace 'quick' --> 'slow': '" // replace(s, 'quick', 'slow') // "'"
    write (*, '(A)') "replace 'fox' --> 'mongoose': '" // replace(s, 'fox', 'mongoose') // "'"
    write (*, '(A)') "replace 'o' --> '0': '" // replace(s, 'o', '0') // "'"
    write (*, '(A)') "replace 'o' --> '0' from back: '" // replace(s, 'o', '0', back=.true.) // "'"
    write (*, '(A)') "replace 'N/A' --> 'nope': '" // replace(s, 'N/A', 'nope') // "'"
  end subroutine test__replace


  subroutine test__capture
    character(len=*), parameter :: s = "The quick, brown fox said 'ooga booga' today."    
    
    call banner("capture")

    write (*, '(A)') "Original string: '" // s // "'"
    write (*, '(A)') "capture between ',' and '.': '" // capture(s, ',', '.')
    write (*, '(A)') "capture between single quotes: '" // capture(s, "'", "'") // "'"
    write (*, '(A)') "capture between 'brown ' and 'a': '" // capture(s, 'brown', 'a') // "'"
    write (*, '(A)') "Greedy capture between 'brown ' and 'a': '" // capture(s, 'brown', 'a', greedy=.true.) // "'"
    write (*, '(A)') "Empty capture between 'z' and '42': '" // capture(s, 'z', '42') // "'"
  end subroutine test__capture


  subroutine test__quote
    character(len=*), parameter :: sq = "This string has 'single' quotes."
    character(len=*), parameter :: dq = 'This string has "double" quotes.'
    character(len=*), parameter :: mq = 'This string''s quotes are "mixed".'
    character(len=*), parameter :: nq = 'This string has no quotes.'

    call banner("quote")

    write (*, '(A)') quote(sq)
    write (*, '(A)') quote(dq)
    write (*, '(A)') quote(mq)
    write (*, '(A)') quote(nq)
  end subroutine test__quote

  subroutine banner(name)
    character(len=*), intent(in) :: name
    
    write (*, '(/,A)') "=== TEST " // name // " " // repeat('=', 80-11-len(name))
  end subroutine banner


end program test_chartools
