module chartools
  implicit none

  private

  public :: is_empty
  public :: is_blank
  public :: as_chars
  public :: as_string
  public :: starts_with
  public :: ends_with
  public :: contains_any
  public :: contains_all
  public :: upcase
  public :: downcase
  public :: strip_left
  public :: strip_right
  !public :: strip ! see FIXME below
  public :: squeeze
  public :: split
  public :: replace
  public :: capture
  public :: quote

  
  character(len=*), parameter :: abc_lower = 'abcdefghijklmnopqrstuvwxyz'
  character(len=*), parameter :: abc_upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  character(len=*), parameter :: digits = '0123456789'
  character(len=*), parameter :: left_delimiters = '([{<|"' // "'"
  character(len=*), parameter :: right_delimiters = ')]}>|"' // "'"


contains

  function is_empty(s)
    !! Test if a string is the empty string.
    logical :: is_empty !! `.true.` if `s` is the empty string, otherwise `.false.`
    character(len=*), intent(in) :: s !! The string to test
    
    is_empty = (len(s) == 0)
  end function is_empty


  function is_blank(s)
    !! Test if a string is blank.
    logical :: is_blank !! `.true.` if `s` contains only spaces, otherwise `.false.`
    character(len=*), intent(in) :: s !! The string to test

    is_blank = (len_trim(s) == 0)
  end function is_blank

  
  function as_chars(s)
    !! Convert a string to an array of length-1 characters. 
    character(len=1), dimension(:), allocatable :: as_chars !! The characters of `s` as an array
    character(len=*), intent(in) :: s !! The string to convert
    
    integer :: i = -1
    as_chars = [(s(i:i), i=1,len(s))]
  end function as_chars

  
  function as_string(c)
    !! Convert an array of length-1 characters to a string.
    character(len=:), allocatable :: as_string !! The elements of `c`, concatenated together
    character(len=1), dimension(:), intent(in) :: c !! The character array to convert
    
    integer :: i =1
    as_string = repeat('x', size(c))
    do i = 1, size(c)
       as_string(i:i) = c(i)
    end do
  end function as_string


  logical function starts_with(string, substring)
    !! Test if a substring matches the start of a string.
    character(len=*), intent(in) :: string !! The string whose start will be tested
    character(len=*), intent(in) :: substring !! The substring to test for
    
    starts_with = (index(string, substring) == 1)
  end function starts_with

  
  function ends_with(string, substring)
    !! Test if a substring matches the end of a string.
    logical :: ends_with !! `.true.` if `string` begins with `substring`, otherwise `.false.`
    character(len=*), intent(in) :: string !! The strings whose end will be tested
    character(len=*), intent(in) :: substring !! The substring to test for
    
    integer :: good_pos 

    good_pos = len(string) - len(substring) + 1
    ends_with = (index(string, substring, back=.true.) == good_pos)
  end function ends_with


  function contains_any(string, set)
    !! Test if a string contains at least one of any character in a set.
    !!
    !! `set` should be a single string, not an array of characters.
    logical :: contains_any
    character(len=*), intent(in) :: string !! The string whose contents will be tested
    character(len=*), intent(in) :: set !! The concatenated set of characters to test for

    ! Should return `.true.` when `set` is the empty string, to match the behavior of
    ! `contains_all`, `starts_with`, and `ends_with`.
    contains_any = (scan(string, set) > 0) .or. is_empty(set)
  end function contains_any
    
  
  logical function contains_all(string, set)
    !! Test if a string contains all characters in a set.
    !! `set` should be a single string, not an array of characters.
    character(len=*), intent(in) :: string !! The string whose contents will be tested
    character(len=*), intent(in) :: set !! The concatenated set of characters to test for
    
    integer :: i = -1
    
    contains_all = .true.
    do i = 1, len(set)          ! edge case: if SET is the empty string, the loop gets skipped
       if (index(string, set(i:i)) == 0) then
          contains_all = .false.
          exit
       end if
    end do
  end function contains_all


  function upcase(s)
    !! Capitalize all lowercase letters of a string.
    character(len=:), allocatable :: upcase !! A copy of `s` with all lowercase letters capitalized
    character(len=*), intent(in) :: s !! The string to capitalize
    
    integer :: i = -1
    integer :: iabc = -1

    upcase = s
    do i = 1, len(s)
       iabc = index(abc_lower, upcase(i:i))
       if (iabc > 0) upcase(i:i) = abc_upper(iabc:iabc)
    end do
  end function upcase


  function downcase(s)
    !! Un-capitalize all uppercase letters of a string.
    character(len=:), allocatable :: downcase !! A copy of `s` with all uppercase letters un-capitalized.
    character(len=*), intent(in) :: s !! The string to un-capitalize.
    
    integer :: i = -1
    integer :: iabc = -1

    downcase = s
    do i = 1, len(s)
       iabc = index(abc_upper, downcase(i:i))
       if (iabc > 0) downcase(i:i) = abc_lower(iabc:iabc)
    end do
  end function downcase


  function strip_left(string, char)
    !! Remove consecutive occurrences of a character from the start of a string.
    character(len=:), allocatable :: strip_left !! A copy of `string` with any leading occurrences of `char` removed
    character(len=*), intent(in) :: string !! The string to strip
    character(len=1), intent(in) :: char !! The character to remove

    character(len=:), allocatable :: tmp
    integer :: num_stripped = -1
    integer :: i = -1

    tmp = string
    num_stripped = 0
    do while ( starts_with(tmp, char) )
       tmp(1:len(tmp)-1) = tmp(2:len(tmp))
       num_stripped = num_stripped + 1
    end do
    
    allocate(character(len=len(string)-num_stripped) :: strip_left)
    do i = 1, len(strip_left)
       strip_left(i:i) = tmp(i:i)
    end do

  end function strip_left

    
  function strip_right(string, char) 
    !! Remove consecutive occurrences of a character from the end of a string.
    character(len=:), allocatable :: strip_right !! A copy of `string` with any trailing occurrences of `char` removed
    character(len=*), intent(in) :: string !! The string to strip
    character(len=1), intent(in) :: char !! The character to remove

    character(len=:), allocatable :: tmp
    integer :: num_stripped = -1
    integer :: i = -1

    tmp = string
    num_stripped = 0
    do while ( ends_with(tmp, char) )
       tmp(2:len(tmp)) = tmp(1:len(tmp)-1)
       tmp(1:1) = ' '
       num_stripped = num_stripped + 1
    end do
    tmp = adjustl(tmp)

    allocate(character(len=len(string)-num_stripped) :: strip_right)
    do i = 1, len(strip_right)
       strip_right(i:i) = tmp(i:i)
    end do
  end function strip_right


  function strip(string, char) 
    !! FIXME: need to ponder this a little more. The idea stripping left and right until
    !! a fixed point is reached seems like a good idea, but I'm not doing it right.
    character(len=:), allocatable :: strip
    character(len=*), intent(in) :: string
    character(len=1), intent(in) :: char
    
    character(len=:), allocatable :: before, after

    before = string
    after = string

    do while (before /= after)
       before = after
       if (allocated(after)) deallocate(after)
       after = strip_left(strip_right(before, char), char)
    end do

    strip = after
  end function strip


  function squeeze(string, char)
    !! Remove duplicate occurrences of a character from a string.
    !! Only adjacent occurrences count as duplicates, e.g., `squeeze('aaba','a') == 'aba'`.
    character(len=:), allocatable :: squeeze !! A copy of `string` with duplicate characters removed
    character(len=*), intent(in) :: string !! The string to squeeze
    character(len=1), intent(in), optional :: char !! The character to squeeze from `string`

    character(len=2) :: char2
    integer :: i = -1, pos = -1, len_squeeze = -1
    integer, dimension(:), allocatable :: keep_idx 

    if ( present(char) ) then
       char2 = char // char
    else
       char2 = '  '
    end if
    

    ! KEEP_IDX holds information about which elements of STRING are to
    ! be retained in the final squeezed result. Each element, i, of KEEP_IDX is
    ! either < 0 -- in which case that element of STRING is a duplicate space 
    ! and is skipped -- or it is a positive integer -- in which case it is the position
    ! of the squeeze string in which to put STRING(i).
    allocate ( keep_idx(len(string)) )
    keep_idx = -1
    
    pos = 0
    do i = 2, len(string)
       if ( string(i-1:i) == char2 ) then
          keep_idx(i) = -2      
       else
          pos = pos + 1
          keep_idx(i) = pos
       end if
    end do

    len_squeeze = maxval(keep_idx)

    allocate( character(len=len_squeeze) :: squeeze ) 
    pos = 0
    do i = 1, len(string)
       if ( keep_idx(i) <= 0 ) then
          cycle
       else
          pos = pos + 1
          squeeze(pos:pos) = string(i:i)
       end if
    end do
  end function squeeze


  subroutine split(string, delimiter, left, right, back)
    !! Split a string around a delimiter into two parts.
    !! The delimiter is not included in either of the resulting parts.
    character(len=*), intent(in) :: string !! The string to split
    character(len=*), intent(in) :: delimiter !! The key that marks the left and right parts of `string`
    character(len=:), allocatable, intent(out) :: left !! The part of `string` before `delimiter`
    character(len=:), allocatable, intent(out) :: right !! The part of `string` after `delimiter`
    logical, intent(in), optional :: back !! If `.true.`, the split occurs around the last occurence of `delimiter` instead of the first (default is `.false.`)

    integer :: pos = -1
    logical :: back_ = .false.

    if (present(back)) back_ = back

    pos = index(string, delimiter, back=back_)

    if ( pos < 1 ) then
       if (back_) then
          left = ''
          right = string
       else
          left = string
          right = ''
       end if
    else
       left = string(:pos-1)
       right = string(pos+len(delimiter):)
    end if
  end subroutine split


  function replace(string, from, to, back)
    !! Within a string, replace the first or last occurence of a substring.
    !! If `from` does not occur in `string`, the result is a copy of `string` unchanged.
    character(len=:), allocatable :: replace !! A copy of `string` with `from` replaced with `to`
    character(len=*), intent(in) :: string !! The string in which replacement will occur
    character(len=*), intent(in) :: from !! The substring of `string` to be replaced
    character(len=*), intent(in) :: to !! The string to replace `from`
    logical, intent(in), optional :: back !! If `.true.`, replace the last occurrence of `from` instead of the first (default is `.false.`)
    
    logical :: back_ = .false.
    character(len=:), allocatable :: left, right

    if (present(back)) back_ = back

    call split(string, from, left, right, back=back_)

    if (is_empty(left) .or. is_empty(right)) then
       ! FROM does not occur in STRING
       replace = string
    else
       replace = left // to // right
    end if
  end function replace


  function capture(string, from, to, greedy)
    !! Capture the part of a string that lies between two keywords.
    !! Neither keyword is included in the result.
    character(len=:), allocatable :: capture !! The substring of `string` between `from` and `to`
    character(len=*), intent(in) :: string !! The string from which to capture
    character(len=*), intent(in) :: from !! The left keyword
    character(len=*), intent(in) :: to !! The right keyword
    logical, intent(in), optional :: greedy !! If `.true.`, then the rightmost occurence of `to` in `string` is used as the right boundary (default is `.false.`)

    integer :: first = -1, last = -1
    logical :: back = .false.
    
    if (present(greedy)) back = greedy
    
    first = index(string, from) + len(from)
    last = index(string(first+1:), to, back) + first - 1
    
    if ( (first < 1) .or. (last < 1) ) then
       capture = ''
    else
       capture = string(first:last)
    end if
  end function capture


  function quote(string)
    !! Surround a string in quotes.
    !! If `string` contains no quotes or only single quotes, it is wrapped in double quotes.
    !! If it contains only double quotes, it is wrapped in single quotes.
    !! If it contains a mix of single and double quotes, it is wrapped in backquotes.
    character(len=:), allocatable :: quote !! A copy of `string` wrapped in quotes.
    character(len=*), intent(in) :: string !! The string to quote
    
    character(len=1), parameter :: double_quote = '"'
    character(len=1), parameter :: single_quote = "'"
    character(len=1), parameter :: back_quote = '`'

    logical :: has_double_quote, has_single_quote
    character(len=1) :: q

    has_double_quote = contains_any(string, double_quote)
    has_single_quote = contains_any(string, single_quote)

    if (has_double_quote .and. has_single_quote) then
       q = back_quote
    else if (has_single_quote) then
       q = double_quote
    else if (has_double_quote) then
       q = single_quote
    else
       q = single_quote
    end if
       
    quote = q // string // q
  end function quote

    
end module chartools
