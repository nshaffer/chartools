Overview
--------

The Fortran runtime library provides only a few intrinsics for 
operating on character variables: `index`, `scan`, `verify`, 
`repeat`and internal `write` statements. `chartools` fills in 
the next level of abstraction with routines to query,
manipulate, and process character variables. Better string 
processing is as simple as `use chartools`.

All routines operate on the system-default character kind, not a
derived string type, and certainly not `iso_varying_string`. On 
most systems the default character kind uses the ASCII character
set, but no such assumption is made in `chartools`. The routines
contained in `chartools` should play nicely whatever the system
(or compiler-overridden) default character kind is.

Where possible, features are implemented as functions instead of
subroutines to minimize the ceremony needed for one-off uses, 
especially in `print` and `write` statements. The preference for
functions over subroutines is based on personal experience 
writing scientific software, where text is mainly used to read 
input, write logs, debug, and emit reports. In these scenarios,
convenience is king.

Installation
------------

All `chartools` routines are housed in a single standalone module.
The included Makefile assumes use of the GNU Fortran compiler,
but any compiler implementing the Fortran 2003 standard should 
work. The main requirement is that it support deferred-length 
character variables (`character(len=:), allocatable :: `), which
are essential to nearly every routine in `chartools`. If you want
to use a different compiler or different options, just edit the
first two lines of the Makefile. There is no `make` wizardry 
going on here; nothing will break. Then run

    make

to build the module and test suite. At the time of this writing 
(v0.1), the tests are informal, but some effort has been made to
consider edge cases. To run the tests, enter

    ./chartools_test

and look at the output to verify that it makes sense. If you have
`valgrind` installed, it is a good idea to run the tests using

    valgrind ./chartools_test
    
to verify that no there are no memory leaks (there will probably
be "still reachable" memory, which is mostly harmless).

If you have FORD installed, you can build documentation with

    make docs

If you don't have FORD, you can obtain it from the Python Package 
Index using `pip` or you can just browse the source of 
`chartools.f90`. It's not exactly a complicated piece of code.
