FC=gfortran
FFLAGS=-g -std=f2003 -Wall -Wextra -pedantic

SOURCES:=chartools.f90
MODULES:=chartools.mod
OBJECTS:=chartools.o

TEST_SRC:=chartools_test.f90
TESTS:=chartools_test

DOCFILE:=ford-setup.md

.PHONY : all tests docs clean info

all : $(OBJECTS) tests

%.o : %.f90
	$(COMPILE.f) -o $@ $^

tests : $(TESTS)

%_test : %_test.f90 $(OBJECTS) 
	$(LINK.f) -o $@ $^

docs : $(DOCFILE) $(SOURCES) 
	-rm -rf doc/
	mkdir doc
	ford $<

clean :
	$(RM) $(MODULES)
	$(RM) $(OBJECTS)
	$(RM) $(TESTS)
	$(RM) -r doc/

info : 
	@echo "Compile CMD: "$(COMPILE.f)
	@echo "Link CMD: "$(LINK.f)
	@echo
	@echo "Sources: "$(SOURCES)
	@echo "Modules: "$(MODULES)
	@echo "Objects: "$(OBJECTS)
	@echo 
	@echo "Test sources: "$(TEST_SRC)
	@echo "Tests: "$(TESTS)
