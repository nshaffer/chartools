src_dir: ./
output_dir: ./doc
project: chartools
summary: `chartools` -- routines for high-level processing on character variables.
version: v0.1
author: Nathaniel R. Shaffer
email: nrs5134@gmail.com
exclude: chartools_test.f90

{!README.md!}
